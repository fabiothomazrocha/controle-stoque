package br.com.gm5.estoque.dto;

import java.util.ArrayList;
import java.util.List;

import br.com.gm5.estoque.model.Item;

public class LojaDTO {
	private String Nome;
	private List<Item> estoque= new ArrayList<>();
	public String getNome() {
		return Nome;
	}
	public void setNome(String nome) {
		Nome = nome;
	}
	public List<Item> getEstoque() {
		return estoque;
	}
	public void setEstoque(List<Item> estoque) {
		this.estoque = estoque;
	}


}
