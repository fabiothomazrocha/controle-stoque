package br.com.gm5.estoque.util;

import java.util.HashMap;
import java.util.Map;


public class MessageBuilder extends HashMap{

    private MessageBuilder() {}

    public static MessageBuilder build() {
        return new MessageBuilder();
    }

    public static MessageBuilder buildErro(Object o) {
        MessageBuilder build = new MessageBuilder();
        build.put("erro", o);
        return build;
    }

    public static MessageBuilder buildMensagem(Object o) {
        MessageBuilder build = new MessageBuilder();
        build.put("message", o);
        return build;
    }
}
