package br.com.gm5.estoque.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table
public class Loja {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotBlank
	@Size(min = 2, max = 40)
	private String Nome;
	@JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties({"loja"})
	@Valid
	@OneToMany(cascade = {CascadeType.PERSIST ,CascadeType.MERGE}, mappedBy = "loja", orphanRemoval = true)
	private List<Item> estoque;

	public Loja(long id, @NotBlank @Size(min = 2, max = 40) String nome, @Valid List<Item> estoque) {
		super();
		this.id = id;
		Nome = nome;
		this.estoque = estoque;
	}

	public Loja() {
		// TODO Auto-generated constructor stub
	}

	public List<Item> getEstoque() {
		return estoque;
	}

	public void setEstoque(List<Item> estoque) {
		this.estoque = estoque;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return Nome;
	}

	public void setNome(String nome) {
		Nome = nome;
	}

	public void addItemEstoque(Item item) {
		if (this.estoque == null) {
			this.estoque = new ArrayList<>();
		}
		item.setLoja(this);
		this.estoque.add(item);
	}
	public void removeItemEstoque(Item item) {
		if (estoque != null) {
			this.estoque.remove(item);
		}
	
		
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Nome == null) ? 0 : Nome.hashCode());
		result = prime * result + ((estoque == null) ? 0 : estoque.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Loja other = (Loja) obj;
		if (Nome == null) {
			if (other.Nome != null)
				return false;
		} else if (!Nome.equals(other.Nome))
			return false;
		if (estoque == null) {
			if (other.estoque != null)
				return false;
		} else if (!estoque.equals(other.estoque))
			return false;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Loja [id=" + id + ", Nome=" + Nome + ", estoque=" + estoque + "]";
	}

}
