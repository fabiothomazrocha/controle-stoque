package br.com.gm5.estoque.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.gm5.estoque.dto.ItemDTO;
import br.com.gm5.estoque.dto.LojaDTO;
import br.com.gm5.estoque.exception.NaoExisteDaoException;
import br.com.gm5.estoque.model.Item;
import br.com.gm5.estoque.model.Loja;
import br.com.gm5.estoque.repository.ItemRepository;
import br.com.gm5.estoque.repository.LojaRepository;

@Service
public class LojaService {
	@Autowired
	public LojaRepository repository;

	@Autowired
	public ItemService itemService;

	public void save(Loja loja, LojaDTO dto) {
		loja.setNome(dto.getNome());
		repository.save(loja);
	}

	public Loja update(Loja loja, LojaDTO dto) {
		lojaValida(loja);
		loja.setNome(dto.getNome());

		return repository.save(loja);
	}

	@Transactional
	public void delete(Loja loja) {
		lojaValida(loja);
		repository.delete(loja);
	}

	public Optional<Loja> findOne(Long id) {
		return repository.findById(id);
	}

	public List<Loja> findAll() {
		return (List<Loja>) repository.findAll();
	}

	public void adicionarItem(Loja loja, Long idItem) {
		lojaValida(loja);
		loja.addItemEstoque(itemService.findOne(idItem));
		repository.save(loja);
	}

	public void excluirItem(Loja loja, Long idItem) {
		lojaValida(loja);
		loja.removeItemEstoque(itemService.findOne(idItem));
		repository.save(loja);
	}

	private void lojaValida(Loja loja) {
		if (loja == null) {
			throw new NaoExisteDaoException("Loja não encontrada");
		}
	}

	public void updateItemLoja(Loja loja, ItemDTO dto, Long idItem) {
		lojaValida(loja);

		Item item = itemService.findOne(idItem);
		item.setNome(dto.getNome());
		item.setValor(dto.getValor());
		itemService.update(item, dto);

	}
}
