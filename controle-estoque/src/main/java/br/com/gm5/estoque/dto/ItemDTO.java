package br.com.gm5.estoque.dto;

public class ItemDTO {
	private String Nome;
	
	private double valor;

	public String getNome() {
		return Nome;
	}

	public void setNome(String nome) {
		Nome = nome;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}


}
