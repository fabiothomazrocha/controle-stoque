package br.com.gm5.estoque.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gm5.estoque.model.Loja;

public interface LojaRepository extends JpaRepository<Loja, Long> {

}
