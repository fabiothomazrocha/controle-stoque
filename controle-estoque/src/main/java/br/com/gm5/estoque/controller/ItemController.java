package br.com.gm5.estoque.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.gm5.estoque.dto.ItemDTO;
import br.com.gm5.estoque.model.Item;
import br.com.gm5.estoque.service.ItemService;
import br.com.gm5.estoque.util.MessageBuilder;

@CrossOrigin
@RestController
@RequestMapping("/item")
public class ItemController {
	@Autowired
	private ItemService service;

	@GetMapping("/itens")
	public List<Item> getAllLojas() {
		return service.findAll();
	}

	@PostMapping("/add-item")
	public ResponseEntity<?> createItem(@Valid @RequestBody ItemDTO dto) {
		Item item = new Item();
		service.save(item, dto);
		return new ResponseEntity(MessageBuilder.buildMensagem("Item criado com sucesso!"), HttpStatus.OK);

	}

	@GetMapping("/{id}")
	public Item getNoteById(@PathVariable(value = "id") Long id) {
		return service.findOne(id);

	}

	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Item alterar(@PathVariable("id") Item item, @RequestBody ItemDTO dto) {
		return service.update(item, dto);
	}

	@DeleteMapping("/{idItem}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void excluir(@PathVariable("idItem") Item item) {
		service.delete(item);
	}
   
}
