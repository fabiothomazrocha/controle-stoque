package br.com.gm5.estoque.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.gm5.estoque.dto.ItemDTO;
import br.com.gm5.estoque.dto.LojaDTO;
import br.com.gm5.estoque.model.Loja;
import br.com.gm5.estoque.service.LojaService;
import br.com.gm5.estoque.util.MessageBuilder;

@CrossOrigin
@RestController
@RequestMapping("/loja")
public class LojaController {
	@Autowired
	private LojaService service;

	@GetMapping("/lojas")
	public List<Loja> getAllLojas() {
		return service.findAll();
	}

	@PostMapping("/add-loja")
	public ResponseEntity<?> createLoja(@Valid @RequestBody LojaDTO dto) {
		Loja loja = new Loja();
		service.save(loja, dto);
		return new ResponseEntity(MessageBuilder.buildMensagem("Loja criada com sucesso!"), HttpStatus.OK);
	}

	@GetMapping("{id}")
	@ResponseStatus(HttpStatus.OK)
	public Optional<Loja> getNoteById(@PathVariable(value = "id") Long id) {
		return service.findOne(id);

	}

	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Loja alterar(@PathVariable("id") Loja loja, @RequestBody LojaDTO dto) {
		return service.update(loja, dto);

	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletar(@PathVariable("id") Loja entity) {
		service.delete(entity);

	}

	@PutMapping("editarItem/{id}/{idItem}")
	@ResponseStatus(HttpStatus.OK)
	public Loja editar(@PathVariable("id") Loja loja, @PathVariable("idItem") Long idItem,
			@RequestBody ItemDTO itemDTO) {

		service.updateItemLoja(loja, itemDTO, idItem);
		return loja;
	}

	@DeleteMapping("removerItem/{id}/{idItem}")
	@ResponseStatus(HttpStatus.OK)
	public Loja removerItem(@PathVariable("id") Loja loja, @PathVariable("idItem") Long idItem) {
		service.excluirItem(loja, idItem);
		return loja;
	}

	@GetMapping("adicionarItem/{id}/{idItem}")
	@ResponseStatus(HttpStatus.OK)
	public Loja adicionarItem(@PathVariable("id") Loja loja, @PathVariable("idItem") Long idItem) {
		service.adicionarItem(loja, idItem);
		return loja;
	}
}
