package br.com.gm5.estoque.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.gm5.estoque.dto.ItemDTO;
import br.com.gm5.estoque.exception.NaoExisteDaoException;
import br.com.gm5.estoque.model.Item;
import br.com.gm5.estoque.model.Loja;
import br.com.gm5.estoque.repository.ItemRepository;

@Service
public class ItemService {
	@Autowired
	public ItemRepository repository;

	public Item save(Item item, ItemDTO dto) {
		item.setNome(dto.getNome());
		item.setValor(dto.getValor());
		return repository.save(item);
	}

	public Item update(Item item, ItemDTO dto) {
		iItemValido(item);
		item.setNome(dto.getNome());
		item.setValor(dto.getValor());
		return repository.save(item);
	}

	@Transactional
	public void delete(Item item) {
		iItemValido(item);
		repository.delete(item);
	}

	public Item findOne(Long id) {
		Optional<Item> item = repository.findById(id);
		if (!item.isPresent()){
			iItemValido(null);
		}
		
		return item.get();
	}

	public List<Item> findAll() {
		return (List<Item>) repository.findAll();
	}

	private void iItemValido(Item item) {
		if (item == null) {
			throw new NaoExisteDaoException("Item não encontrado");
		}
	}
}
