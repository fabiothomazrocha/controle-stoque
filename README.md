# controle-stoque

Instruções Controle de Estoque
 
A aplicação foi criada utilizando Spring Boot, MySql e Jsonwebtoken.

Para rodar a aplicação importe para sua IDE  como um Maven project.
 E iniciar a classe : ControleEstoqueApplication.java. 

Ou você pode gerar um pacote com o Maven:

maven package
mvn install

Rodar o Jar.
java -jar target/controle-estoque-0.0.1-SNAPSHOT.jar

Por favor antes de testar configurar conexão com o banco de dados, que está no arquivo de propriedades.

Para acessar aplicação você precisa : http://localhost:9090/login via (POST) 
para fazer o login, 
inserindo o usuário default da aplicação:
 {"username": "admin", "password":"gm5"} 
 o mesmo irá  gerar um Token para acesso da API.

Lista de endpoints de Loja.

Criar loja: http://localhost:9090/loja/add-loja (POST)
Lista todas as lojas : http://localhost:9090/loja/lojas (GET)
Edita uma loja : http://localhost:9090/loja/{id} (PUT)
Exclui uma loja : http://localhost:9090/loja/{id} (DELETE)
 
Adiciona um item a uma loja: http://localhost:9090/loja/adicionarItem/{idLoja}/{idItem}
Edita um item da loja : http://localhost:9090/loja/editarItem/{idLoja}/{idItem} (PUT)
Exclui um item da loja : http://localhost:9090/loja/removerItem//{idLoja}/{idItem} (DELETE)

Lista de endpoints de Item.

Criar item: http://localhost:9090/item/add-item (POST)
Lista todas os itens : http://localhost:9090/item/itens (GET)
Edita uma loja : http://localhost:9090/item/{id} (PUT)
Exclui uma loja : http://localhost:9090/item/{id} (DELETE)

